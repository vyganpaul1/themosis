<?php

// DT: pagal užduotį reikėjo sukurti naują WordPress Custom Post Type įrašų tipą 'movies'

$movies = PostType::make('movies', 'Movies', 'movie')->set([
    'public'    => true,
    'supports'  => [
        'title',
        'editor',
        'excerpt',
        'thumbnail'
    ],
]);