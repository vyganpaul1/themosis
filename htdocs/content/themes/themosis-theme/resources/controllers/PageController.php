<?php

namespace Theme\Controllers;
use Themosis\Route\BaseController;

/**
 * DT: visų puslapių/įrašų atvaizdavimą patogu turėti viename controller. Jei puslapių/įrašų tipų daug, tada galima skaidyti į atskirus controllerius.
 */
class PageController extends BaseController
{
    public function home()
    {
    	// render a homepage
    }

    public function singleMovie(WP_Post $post)
    {
    	// render a single movie record
    }

    public function archiveMovie(WP_Post $post)
    {
    	// render a list of movies
    }

    public function page(WP_Post $post)
    {
    	// render a general WordPress page
    }

    public function pageContact(WP_Post $post)
    {
    	// render a page that uses Contact template
    }

    // ...
}

