<?php

namespace Theme\Controllers;
use Themosis\Route\BaseController;

class Home extends BaseController
{
    public function index()
    {

        return view ('pages.home');
    }
}

