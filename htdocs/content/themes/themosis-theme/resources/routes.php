<?php
use Themosis\Facades\Route;
use Theme\Models\Post;

/**
 * Define your routes and which views to display
 * depending of the query.
 *
 * Based on WordPress conditional tags from the WordPress Codex
 * http://codex.wordpress.org/Conditional_Tags
 *
 */


Route::get ('front', function (Post $model, \WP_Post $post) {

    $imageData = get_the_post_thumbnail ('full');
    return view ('pages.home', [
        'image' => $imageData,
        'name' => 'Home',
        'post' => $post,
        'posts' => $model->list_all ()  // DT: pagal užduotį buvo reikalavimas atvaizduoti TOP 3. Jei būtų 1000 įrašų, tai 1000 ir krautų į homepage.
    ]);
});

Route::any ('page', ['contact', function () {
    return view ('pages.contact', [
        'contact' => 'Contact us',  // DT: 'title' turbūt būtų tikslesnis kintamojo pavadinimas
    ]);
}]);

Route::get ('page', ['movies', function (Post $model) { // DT: movies sąrašas turėtų būti aprašomas per 'postTypeArchive' route [1]
//    $image_array = wp_get_attachment_image_src (get_post_thumbnail_id ($model->id), 'medium');

    return view ('pages.movies', [
//        'image' => $image_array,
        'name' => 'Movies list',
        'posts' => $model->list_all (),
    ]);

}]);


Route::get ('movies/{id}', function ($id) { // DT: vienas movies įrašas turėtų būti pasiekiamas per 'singular' route [2]
    return "movie with ID {$id}";
});

// [1]
Route::any('singular', ['movies', 'uses' => 'PageController@singleMovies']);
// [2]
Route::any('postTypeArchive', ['movies', 'uses' => 'PageController@archiveMovies']);

Route::get ('single', function (\WP_Post $post) { // DT: šis route atvaizduojama bet kokį įrašą, kurio post_type='post'. Norint atvaizduoti bet kokį puslapį, kaip įrašyta užduotyje, reikėtų naudoti: Route::any('page', 'PageController@page');
    return view ('pages.movie-single', [
        'post' => $post
    ]);
});

Route::get ('category', function () {
    return 'Category term posts';
});