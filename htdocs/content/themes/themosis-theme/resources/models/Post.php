<?php

namespace Theme\Models;

use WP_Query;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    protected $table = 'posts';
    protected $primaryKey = 'ID';
    public $timestamps = false;

    public function list_all(int $limit = -1) // DT: grąžinamų įrašų limitą būtų galima turėti taip
    // public function list_all()
    {
        $query = new WP_Query([
            'post_type' => 'post',
            'posts_per_page' => $limit,
            'post_status' => 'publish'
        ]);

        return $query->get_posts ();

    }
    public function meta () {
        Metabox::make('Additional Information', 'page', [
            'context'   => 'normal'
        ])->set([
            Field::editor('content-lead'),
        ]);

        Metabox::make('Random Metabox Title', 'page', [
            'context'  => 'normal',
            'priority' => 'high'
        ])->set([
            Field::infinite('something', [
                Field::text('title'),
                Field::textarea('content')
            ], [
                'title' => 'Title',
                'limit' => 10
            ])
        ]);


    }

}
