<!DOCTYPE html>
<html <?php language_attributes (); ?>>
<head>
    <meta charset="utf-8">
    {{-- DT: title nereikia, nes jį sugeneruos wp_head() funkcija iškviečiama žemiau --}}
    {{-- <title>Bootswatch: Lumen</title> --}}
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link rel="stylesheet" href="https://bootswatch.com/4/litera/bootstrap.css" media="screen">
    <link rel="stylesheet" href="https://bootswatch.com/_assets/css/custom.min.css" media="screen">
    <?php wp_head (); ?>
</head>
<body  <?php body_class(); ?>>
<nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-primary">
    <div class="container">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav"
                aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <?php
        wp_nav_menu(array(
            'theme_location'    => 'header-nav',
            'container'       => 'div',
            'container_id'    => 'main-nav',
            'container_class' => 'collapse navbar-collapse',
            'menu_id'         => false,
            'menu_class'      => 'navbar-nav',
            'depth'           => 2,
            'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
            'walker'          => new wp_bootstrap_navwalker()
        ));
        ?>

    </div>
</nav>

