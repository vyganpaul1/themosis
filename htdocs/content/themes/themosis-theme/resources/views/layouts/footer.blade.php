<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">

                <ul class="list-unstyled">
                    <li class="float-lg-right"><a href="#top">Back to top</a></li>

                </ul>


            </div>
        </div>
    </div>
</footer>
<?php wp_footer() ?>
</body>
</html>