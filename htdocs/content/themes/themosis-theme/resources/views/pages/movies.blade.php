@extends('layouts.main')

@section('main')

    <div class="jumbotron">
        {{-- DT: antraštę būtų galima iškelti į layouts failą, kurį naudoja visi views, nes tas pats kodas kartojasi per skirtingus views --}}
        <h1 class="display-3">{{ $name }}</h1>
    </div>
    <section class="showcase">
        <div class="container-fluid p-0">
            <?php $count = 0; ?>
            @foreach($posts as $post)
                <?php
                $image_array = get_the_post_thumbnail_url (($post->ID), 'full');
                    $excerpt = $post->post_content;
                ?>
                <?php $count++; ?>
                <?php if ($count % 2 == 0) :  ?>

                <div class="row even no-gutters">
                    <div class="col-lg-6 text-white showcase-img d-block d-sm-block d-lg-none d-xl-none">
                        <img class="float-right" src="{!!$image_array!!}">
                    </div>
                    <div class="col-lg-6  my-auto showcase-text">
                        <h2 class="title"> {{ $post->post_title }}</h2>

                        <p class="lead mb-0">
                            <?php
                            //echo mb_strimwidth ($excerpt, 0, 350, '... <p><a class="btn btn-secondary" href="' . WP_SITEURL . '/' . $post->post_name . '"><span>Read more</span> </a></p>');
                            ?>
                            {{-- DT --}}
                            <a class="btn btn-secondary" href="{{ get_permalink($post) }}">{{ $post->post_title }}</a>
                        </p>

                    </div>
                    <div class="col-lg-6 text-white showcase-img d-none d-lg-block d-xl-none d-xl-block">
                        <img class="float-right" src="{!!$image_array!!}">
                    </div>
                </div>
                <?php else: ?>
                <div class="row odd no-gutters">

                    <div class="col-lg-6 float-left text-white showcase-img">
                        <img class="float-left" src="{!!$image_array!!}">
                    </div>
                    <div class="col-lg-6  my-auto showcase-text">
                        <h2 class="title"> {{ $post->post_title }}</h2>

                        <p class="lead mb-0">


                            <?php
                            // DT: Kodėl toks sprendimas?
                            // echo mb_strimwidth ($excerpt, 0, 350, '... <p><a class="btn btn-secondary" href="' . WP_SITEURL . '/' . $post->post_name . '"><span>Read more</span> </a></p>');
                            ?>

                            {{-- DT: 'Read more' mygtuko generavimas --}}
                            <a class="btn btn-secondary" href="{{ get_permalink($post) }}">Read more</a>

                        </p>
                    </div>
                </div>
                <?php endif; ?>
            @endforeach
        </div>
    </section>

@endsection
