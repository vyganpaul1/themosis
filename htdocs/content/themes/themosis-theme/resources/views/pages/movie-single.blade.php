@extends('layouts.main')

@section('main')

    <?php
    $array = get_field ('year');
    $image_array = get_the_post_thumbnail_url (($post->ID), 'medium_large');
    ?>
    <div class="single-page">
        <div class="row">
            <div class="col-lg-12 text-white showcase-img">
                <img src="{!!$image_array!!}">
            </div>
        </div>
        <h2 class="title"> {{ $post->post_title }}</h2>
        <?php if (get_field ("year")) { ?>
        <p><strong>Year:</strong>{!! $array !!}</p>
        <?php } ?>
        <p> {{ $post->post_content }}</p>
    </div>
@endsection
