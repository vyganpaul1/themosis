@extends('layouts.main')

@section('main')

    <div class="jumbotron">
    	{{-- DT: antraštę būtų galima iškelti į layouts failą, kurį naudoja visi views, nes tas pats kodas kartojasi per skirtingus views --}}
        <h1 class="display-3">{{$contact}}</h1>
    </div>
    <?php echo do_shortcode ('[contact-form-7 id="34" title="Contact form 1"]'); ?>


@endsection
