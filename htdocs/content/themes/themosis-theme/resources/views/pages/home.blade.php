@extends('layouts.main')

@section('main')

    <div class="jumbotron">
        {{-- DT: antraštę būtų galima iškelti į layouts failą, kurį naudoja visi views, nes tas pats kodas kartojasi per skirtingus views --}}
        <h1 class="display-3">{{ $name }}</h1>
    </div>
    <section class="testimonials text-center bg-light">
        <div class="container">
            <div class="row">

                @foreach($posts as $post)
                    <?php
                    $excerpt = $post->post_content;
                    $image = get_the_post_thumbnail_url (($post->ID), 'medium');
                    ?>
                    <div class="col-lg-4">
                        <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                            <img class="img-fluid  mb-3" src="{{$image}}" alt="">
                            <h5>
                                {{-- <a href="<?php echo WP_SITEURL . '/' . $post->post_name; ?>"> {{ $post->post_title }}</a> --}}
                                {{-- DT --}}
                                <a class="btn btn-secondary" href="{{ get_permalink($post) }}">{{ $post->post_title }}</a>
                            </h5>
                            <p class="lead mb-0">
                                <?php
                                // DT: Kodėl toks sprendimas?
                                // echo mb_strimwidth ($excerpt, 0, 350, '... <p><a class="btn btn-secondary" href="' . WP_SITEURL . '/' . $post->post_name . '"><span>Read more</span> </a></p>');
                                ?>

                                {{-- DT: 'Read more' mygtuko generavimas --}}
                                <a class="btn btn-secondary" href="{{ get_permalink($post) }}">Read more</a>
                            </p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

@endsection
